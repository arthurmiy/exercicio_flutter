import 'package:codigo_base/main.dart';
import 'package:codigo_base/widgets/my_custom_button.dart';
import 'package:codigo_base/widgets/my_custom_card.dart';
import 'package:flutter/material.dart';

class ConversionPage extends StatefulWidget {
  static final String route = '/conv';
  @override
  _ConversionPageState createState() => _ConversionPageState();
}

class _ConversionPageState extends State<ConversionPage> {
  late TextEditingController text;
  String? errorMsg;

  @override
  void initState() {
    super.initState();
    text = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    //similar to the main page
    final bool isHeight = ModalRoute.of(context)?.settings.arguments as bool;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Conversor',
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //todo utilizar o cartão customizado criado
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Valor a ser convertido',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    TextField(
                      controller: text,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        errorText: errorMsg,
                        hintText: 'Digite o valor que deseja converter',
                        labelText: 'Valor',
                        suffix: Text(_getUnit(isHeight)),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    MyCustomButton('Converter', onPress: () {
                      double? tmp = _convertValue(isHeight);
                      if (tmp != null) {
                        Navigator.pop(context, tmp);
                      } else {
                        setState(() {
                          errorMsg = 'Digite um valor válido';
                        });
                      }
                    })
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _getUnit(bool isHeight) {
    String returnValue = '';
    if (isHeight) {
      returnValue = isMetric ? 'ft' : 'm';
    } else {
      returnValue = isMetric ? 'lbs' : 'kg';
    }
    return returnValue;
  }

  double? _convertValue(bool isHeight) {
    errorMsg = null;
    double? returnValue;
    double? typedValue = double.tryParse(
        text.text); //pega valor da caixa de texto (nulo se for invalido)
    if (typedValue == null) {
      errorMsg = 'Valor inválido';
      return null;
    }
    //todo converter valores de acordo com o sistema metrico selecionado e a variavel isHeight
    return returnValue;
  }
}
