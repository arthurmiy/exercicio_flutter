import 'dart:math';

import 'package:codigo_base/main.dart';
import 'package:codigo_base/widgets/my_custom_button.dart';
import 'package:codigo_base/widgets/my_custom_card.dart';
import 'package:flutter/material.dart';

class IMCCalculator extends StatefulWidget {
  //todo declarar nome de rota

  @override
  _IMCCalculatorState createState() => _IMCCalculatorState();
}

class _IMCCalculatorState extends State<IMCCalculator> {
  //variables declaration
  late TextEditingController weight;
  String? weightErrorMsg;

  late TextEditingController height;
  String? heightErrorMsg;

  double? result;

  @override
  void initState() {
    super.initState();
    height = TextEditingController();
    weight = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            //todo opcional: incluir imagem
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Text(
                'Calculadora de IMC',
              ),
            ),
          ],
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () async {
                bool tmp = isMetric; //salva configurações antigas
                //todo chamar página de configuração
                if (tmp != isMetric)
                  _clear(); //limpa tela se a configuração mudou
                setState(() {});
              })
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                //todo utilizar card customizado
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      'Dados',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          child: TextField(
                            controller: weight,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                errorText: weightErrorMsg,
                                hintText: 'Digite seu peso',
                                labelText: 'Peso',
                                suffix: isMetric ? Text('kg') : Text('lbs')),
                          ),
                        ),
                        IconButton(
                            tooltip: 'Converter peso',
                            icon: Icon(Icons.track_changes),
                            onPressed: () => _convert(false))
                      ],
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            controller: height,
                            decoration: InputDecoration(
                                errorText: heightErrorMsg,
                                hintText: 'Digite sua altura',
                                labelText: 'Altura',
                                suffix: isMetric ? Text('m') : Text('ft')),
                          ),
                        ),
                        IconButton(
                          tooltip: 'Converter altura',
                          icon: Icon(Icons.track_changes),
                          onPressed: () => _convert(true),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    MyCustomButton('Calcular meu IMC', onPress: _calculateIMC),
                    SizedBox(
                      height: 40,
                    ),
                    MyCustomButton(
                      'Limpar Campos',
                      onPress: _clear,
                      icon: Icons.remove_circle,
                    )
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                AnimatedContainer(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    //You can put conditionals in a single line as following
                    //variable = condition ? ifTrueValue : ifFalseValue;
                    color: result == null
                        ? Colors.transparent
                        : result! < 17 || result! > 25
                            ? Theme.of(context).errorColor
                            : Colors.green,
                  ),
                  duration: Duration(milliseconds: 500),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Resultado: ${result == null ? '-' : result?.round()}',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /*
      When the name of a function begins with underline it means that this function is private (is not visible from outside the class)
   */
  void _calculateIMC() {
    //reset previous values
    weightErrorMsg = null;
    heightErrorMsg = null;
    result = null;
    setState(() {});

    //sets error msg
    if (weight.text.isEmpty) weightErrorMsg = 'Peso é obrigatório';
    if (height.text.isEmpty) heightErrorMsg = 'Altura é obrigatória';

    if (weight.text.isNotEmpty && height.text.isNotEmpty) {
      //if not empty
      //verify weight
      double? weightTmp = double.tryParse(weight.text);
      if (weightTmp == null) {
        setState(() {
          weightErrorMsg = 'Valor inválido de peso';
        });

        return;
      }

      //verify weight
      double? heightTmp = double.tryParse(height.text);
      if (heightTmp == null) {
        setState(() {
          heightErrorMsg = 'Valor inválido de altura';
        });
        return;
      }

      //conversion
      weightTmp = isMetric ? weightTmp : weightTmp / 2.2;
      heightTmp = isMetric ? heightTmp : heightTmp / 3.28;

      result = weightTmp / pow(heightTmp, 2);
    }

    //rebuild to reflect changes in the interface
    setState(() {});
  }

  Future<void> _convert(bool isHeight) async {
    //todo chamar página de conversão e pegar valores

    //todo converter valores e atribuir ao respectivo campo

    setState(() {});
  }

  void _clear() {
    setState(() {
      height.text = '';
      weight.text = '';
      result = null;
    });
  }
}
