import 'package:codigo_base/main.dart';
import 'package:codigo_base/widgets/my_custom_button.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  //todo colocar nome da rota

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Configurações'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Column(
            children: [
              //todo utilizar o cartão customizado criado
              Row(
                children: [
                  Checkbox(
                      value: isMetric,
                      onChanged: (v) {
                        setState(() {
                          //todo atualizar controle de sistema métrico
                          //todo salvar alterações na memória (biblioteca importada)
                        });
                      }),
                  SizedBox(
                    width: 10,
                  ),
                  Text('Sistema métrico')
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
