import 'package:flutter/material.dart';

//determina o sistema atual
bool isMetric = true;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  //todo  importar biblioteca "Shared Preferences Settings
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'IMC Calculator',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //todo mudar para named routes
      home: MyHomePage(),
    );
  }
}

//todo excluir:
class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
