import 'package:flutter/material.dart';

class MyCustomCard extends StatelessWidget {
  final Widget? child;

  MyCustomCard({this.child});
  @override
  Widget build(BuildContext context) {
    //todo criar cartão customizado de acordo com sua preferência
    return child ?? Container();
  }
}
