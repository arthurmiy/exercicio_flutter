import 'package:flutter/material.dart';

class MyCustomButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final GestureTapCallback? onPress;

  MyCustomButton(this.text,
      {this.icon = Icons.calculate_outlined, @required this.onPress});

  @override
  Widget build(BuildContext context) {
    //todo criar botão customizado
    return Container();
  }
}
